# QR Code Generation Process

This is the personal journal for the project, here I going to give details about de QRCode Standard, and my progress in this adventure

## 1. Data Analysis

The QR Code have certain code mode, the most important are:

__Numeric Mode__: Encodes only numbers (0-9)

__Alpha__: Encodes the alphanumeric values (A-Z0-9+-/:,*)

__ByteMode__: By default encodes the ISO-8859-1 charset ([Latin-1 Charset](https://kb.iu.edu/d/aepu))

__Kanji Mode__: Double byte mode, this takes two bytes and encodes the Kanji Characters more efficiently

The most rare modes are:

__ECI__: Specifies the (UTF-8) Charset, but not all QR Readers can interpret the information in the code.

__EAM__: Encodes data along 16 different QRCodes

__FNCI1__: Allows to encode GS1 Bar codes [GS1](https://es.wikipedia.org/wiki/GS1)

---

### Lets Go To The Encoding

![GS1 Barcode](imgs/fci1.png)

---

## 2. Encoding

The encoding have several steps, I will describe them here.

### 1. Step One: Choose The mode

Choose the mode: First we have to choose a encoding mode, to get started I choose the alpha numeric mode, in the end of the project I going to code the remaining methods. We have to add the method to the encoded resulting string, the mode codes are the following.

- 0001 (1): Numeric Mode
- 0010 (2): Alpha Numeric Mode
- 0100 (3): Byte Mode
- 1000 (4): Kanji Mode

All the modes have a specific way to encode the string, for the alpha numeric mode (A-Z0-9) + symbols, the first parameter that you have to encode is the string length, the default length of the string is specific depending of the __version__ of the qr code and the __mode__ used, in my case is 9 bits length string.

### 2. Encode the length

To encode the length of the string, first you need to convert the length to binary, for example __"HELLO WORLD"__  have 11 characters long, in binary is: __1011__, but the binary length just have 4 bits and we need 9 to encode AlphaNumeric in the first version then we need to add zeros to the left up to have a 8 bits string, then the result should be __000001011__

### 3. Encode the Data

To encode the data, we need the alpha numeric table (alpha.table file) that maps a character to a numeric value, then we need to break up the word in pairs, and if the number of characters is odd the last character is coded in the end, then our string broke into pairs will look like this: __"HE LL O  WO RL D"__ (the spaces also count as characters), now we encode the pairs into ASCII codes

H -> 17
E -> 14

Then we need to multiply the __first character of the pair__ by 45 and add the value of the second character

(45 * 17) + 14 = 779

Then we encode them in binary:

779 -> 1100001011

And we need resulting string is 11 character long, so we pad with zeros in the left up to have 11 characters

779 -> 1100001011

If the length of characters is odd, we need to take the binary representation of the last character and convert then to a 6-bits binary string

D -> 13 -> 1101 -> 001101

Then we encode all the string and the result encoded data should be like this:

|  __Data Mode__ | __String Length__  | __String Data Encoded__ |  
|---|---|---|
| 0010  |  000001011 | 01100001011 01111000110 10001011100 10110111000 10011010100 001101 |

### 4. Breaking Up to 8-bits CoreWords

After obtain this result, we need to break the string into 8 bits long core words, but before, we need to add the terminator, the terminator, first we need to determine the final bit length of our string, in this case for a QR code __Version 1 Q Error Correction__ we need 104 bits, this bits lengths are fixed, but ¿what is error correction?

#### 4.1 Error Correction

QRCodes uses Reed Solomon error correction. this process creates error correction corewords based in our encoded data, the QR Code Reader uses the error correction to determine if it can't read data correctly, there are 4 levels of error correction which are the following:

| __Error Correction Level__  | __Error Correction Capability__  |
|---|---|
| L  |  Recovers 7% of data  |
| M  |  Recovers 15% of data |
| Q  | Recovers 25% of data  |
| H | Recovers 30% of data |

Be aware higher levels of correction requires more bytes

### 4. Breaking Up to 8-bits CoreWords (Part 2)

Now we have to determine the terminator, this is determined as follows: if the encoded data is minor shorted than 104 bits we add 4 zeros at the end of the string otherwise we add the remaining quantity to get 104 bits. Our string now have 78 bits long, and should be like this

|  __Data Mode__ | __String Length__  | __String Data Encoded__ |  __Terminator__ |
|---|---|---|---|
| 0010  |  000001011 | 01100001011 01111000110 10001011100 10110111000 10011010100 001101 | 0000 |

Now we can break the string into 8 bit corewords, and the string should look like this:

00100000 01011011 00001011 01111000 11010001 01110010 11011100 01001101 01000011 010000

Then the last coreword have only 6 characters, then we need to add the remaining bits to make it 8 bits long:

00100000 01011011 00001011 01111000 11010001 01110010 11011100 01001101 01000011 010000__00__

### 5. The last padding

If our string it not yet 104 bits long, if you need less than 8 bits for reach the 104 bits, you need to fill with zeros, other wise we need to fill with the 8 bits binary representations of the numbers 236 and 17, 11101100 and 00010001 respectively, the encoded string, at this point look like this: 

 00100000 01011011 00001011 01111000 11010001 01110010 11011100 01001101 01000011 01000000 11101100 00010001 11101100

### 6. My program

The encoding in my program is finished, and the outputs are the following

![GS1 Barcode](imgs/me.png)

This is a educational program, all the backend is open in console, and the different parts of the process will be echoed to the console.
