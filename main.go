package main

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	numericMode = "0001"
	alphaMode   = "0010"
	byteMode    = "0100"
	kanjiMode   = "1000"

	_ = iota
	L = iota
	M = iota
	Q = iota
	H = iota
)

type version struct {
	versionNumber  uint8
	versionModules int
	numericMode    uint8
	alphaMode      uint8
	byteMode       uint8
	kanjiMode      uint8
}

type errorCorrectionBlock struct {
	blocksRequired            uint8
	generatoPolynomial        []int
	generatorPolynomialString string
}

func getErrorPolynomials(version uint8, level uint) errorCorrectionBlock {
	if version == 1 {
		if level == L {
			return errorCorrectionBlock{
				blocksRequired:            7,
				generatoPolynomial:        []int{0, 87, 229, 146, 149, 238, 102, 21},
				generatorPolynomialString: "α0x7 + α87x6 + α229x5 + α146x4 + α149x3 + α238x2 + α102x + α21",
			}
		} else if level == M {
			return errorCorrectionBlock{
				blocksRequired:            10,
				generatoPolynomial:        []int{0, 251, 67, 46, 61, 118, 70, 64, 94, 32, 45},
				generatorPolynomialString: "α0x10 + α251x9 + α67x8 + α46x7 + α61x6 + α118x5 + α70x4 + α64x3 + α94x2 + α32x + α45",
			}
		} else if level == Q {
			return errorCorrectionBlock{
				blocksRequired:            13,
				generatoPolynomial:        []int{0, 13, 74, 152, 176, 100, 86, 100, 106, 104, 130, 218, 206, 140, 78},
				generatorPolynomialString: "α0x13 + α74x12 + α152x11 + α176x10 + α100x9 + α86x8 + α100x7 + α106x6 + α104x5 + α130x4 + α218x3 + α206x2 + α140x + α78",
			}
		} else if level == H {
			return errorCorrectionBlock{
				blocksRequired:            17,
				generatoPolynomial:        []int{0, 43, 139, 206, 78, 43, 239, 123, 206, 214, 147, 24, 99, 150, 39, 243, 163, 136},
				generatorPolynomialString: "α0x17 + α43x16 + α139x15 + α206x14 + α78x13 + α43x12 + α239x11 + α123x10 + α206x9 + α214x8 + α147x7 + α24x6 + α99x5 + α150x4 + α39x3 + α243x2 + α163x + α136",
			}
		}
	}

	return errorCorrectionBlock{}
}

type qrCode struct {
	v                        version
	message                  string
	dataMode                 string
	dataLen                  string
	encodedData              string
	terminator               string
	totalLength              uint
	errorMode                uint
	bitsRequired             uint
	lenCoreWords             uint
	finalLength              uint
	coreWords                [][8]string
	coreWordsInt             []int
	ecWords                  []int
	coreWordsIntStr          string
	coreWordsString          string
	finalString              string
	errorCorrection          *errorCorrectionBlock
	interleavedMessageSlice  []string
	InterleavedMessage       string
	InterleavedMessagePretty string
	matrixFinders            [21][21]Protected
	matrixSeparators         [21][21]Protected
	matrixTiming             [21][21]Protected
	matrixDarkBlock          [21][21]Protected
	matrixReserved           [21][21]Protected
	matrixData               [21][21]Protected
	bestMatrix               [21][21]Protected
	bestScore                int
	bestIndex                int
	preOutput                [21][21]Protected
}

func NewQRCode(v uint8, errorMode uint, message string) (qr *qrCode) {
	var ver version
	makeTable()

	switch v {
	case 1:
		ver = version{v, 21, 10, 9, 8, 8}
	case 10:
		fallthrough
	case 27:
		fallthrough
	default:
		fmt.Printf("Version doesn't exists current versions: 1, 10, 27")
	}
	errPl := getErrorPolynomials(v, errorMode)
	qr = &qrCode{v: ver, errorMode: errorMode, message: message, errorCorrection: &errPl}
	qr.encodeLen(message, 2)
	qr.makePairs(message)
	qr.errorCalculator()
	qr.getTerminator()
	qr.getTotalLength()
	qr.MakeCoreWords()
	qr.finalizeString()
	qr.coreWordsToDecimals()
	ecWords := MakeLongDivision(len(qr.coreWordsInt), qr.coreWordsInt, errPl.generatoPolynomial)
	qr.ecWords = ecWords
	qr.InterLeave()
	matrix := MakeMatrix()
	dim := qr.v.versionModules

	MakeFinderPatterns(&matrix, 0, 0)
	MakeFinderPatterns(&matrix, dim-7, 0)
	MakeFinderPatterns(&matrix, 0, dim-7)
	qr.matrixFinders = matrix
	addSeparator(&matrix, 7, 7, dim)
	addSeparator(&matrix, dim-8, 7, dim)
	addSeparator(&matrix, 7, dim-8, dim)
	qr.matrixSeparators = matrix
	addTimingLine(&matrix, dim)
	qr.matrixTiming = matrix
	AddDarkModule(&matrix, 8, int(4*qr.v.versionNumber+9))
	qr.matrixDarkBlock = matrix
	ReserveFormatInfoArea(&matrix, dim)
	qr.matrixReserved = matrix
	fillIntoMatrix(&matrix, qr.InterleavedMessage, dim)
	qr.matrixData = matrix
	qr.evalMask()

	qr.encodeInfoBits()
	return
}

func (q *qrCode) MakeCoreWords() {
	entireString := q.dataMode + q.dataLen + q.encodedData + q.terminator
	entireStringFmt := strings.ReplaceAll(entireString, " ", "")
	runeString := []rune(entireStringFmt)
	coreWord := [8]string{}
	sliceWord := []string{}
	for i := range runeString {
		if i%8 == 0 && i != 0 {
			q.coreWordsString += " "
			copy(coreWord[:], sliceWord)
			q.coreWords = append(q.coreWords, coreWord)
			coreWord = [8]string{}
			sliceWord = []string{}
		}
		sliceWord = append(sliceWord, string(runeString[i]))
		q.coreWordsString += string(runeString[i])
	}
	for {
		if len(sliceWord) != 8 {
			sliceWord = append(sliceWord, "0")
			q.coreWordsString += "0"
		} else {
			break
		}
	}
	coreWord = [8]string{}
	copy(coreWord[:], sliceWord)
	q.coreWords = append(q.coreWords, coreWord)

}

func (q *qrCode) padZeros(bits uint8, s string) (res string) {
	var d []int
	if uint8(len(s)) == bits {
		return
	}
	for _, v := range s {
		switch v {

		case '0':
			d = append(d, 0)
		case '1':
			d = append(d, 1)
		}
	}
	for uint8(len(d)) != bits {
		d = append([]int{0}, d...)
	}

	res = ""
	for _, v := range d {
		res += strconv.Itoa(v)
	}

	return
}

func (q *qrCode) encodeLen(message string, mode int) {
	var bits uint8
	var prefix string
	switch mode {
	case 1:
		bits = q.v.numericMode
		prefix = numericMode
	case 2:
		bits = q.v.alphaMode
		prefix = alphaMode
	case 3:
		bits = q.v.byteMode
		prefix = byteMode
	case 4:
		bits = q.v.kanjiMode
		prefix = kanjiMode
	}
	s := fmt.Sprintf("%b", len(message))
	res := q.padZeros(bits, s)
	q.dataLen = res
	q.dataMode = prefix
}

func (q *qrCode) makePairs(message string) {
	var pair [][]rune
	var binStr string
	if len(message)%2 == 1 {
		messageTemp := []rune(message)
		last := AlphaTable[string(messageTemp[len(messageTemp)-1])]
		binStr = fmt.Sprintf("%b", last)
		message = string(messageTemp[:len(messageTemp)-1])
	}
	for i := 2; i <= len(message); i += 2 {
		pair = append(pair, []rune(message)[i-2:i])
	}

	for _, v := range pair {
		numberEncoded := (45 * int(AlphaTable[string(v[0])])) + int(AlphaTable[string(v[1])])
		binNumber := fmt.Sprintf("%b", numberEncoded)
		if len(binNumber) == 11 {
			q.encodedData += " " + binNumber
		} else {
			q.encodedData += " " + q.padZeros(11, binNumber)
		}
	}
	if len(binStr) < 6 {
		q.encodedData += " " + q.padZeros(6, binStr)
	}

}

func (q *qrCode) errorCalculator() {
	if q.v.versionNumber == 1 {
		if q.errorMode == L {
			q.bitsRequired = 19 * 8
		} else if q.errorMode == M {
			q.bitsRequired = 16 * 8
		} else if q.errorMode == Q {
			q.bitsRequired = 13 * 8
		} else if q.errorMode == H {
			q.bitsRequired = 9 * 8
		}
	} else if q.v.versionNumber == 10 {
		if q.errorMode == L {
			q.bitsRequired = 274 * 8
		} else if q.errorMode == M {
			q.bitsRequired = 216 * 8
		} else if q.errorMode == Q {
			q.bitsRequired = 154 * 8
		} else if q.errorMode == H {
			q.bitsRequired = 122 * 8
		}
	} else if q.v.versionNumber == 27 {
		if q.errorMode == L {
			q.bitsRequired = 1468 * 8
		} else if q.errorMode == M {
			q.bitsRequired = 1128 * 8
		} else if q.errorMode == Q {
			q.bitsRequired = 808 * 8
		} else if q.errorMode == H {
			q.bitsRequired = 628 * 8
		}
	}
}

func (q *qrCode) getTotalLength() {
	q.totalLength = uint(len(q.dataMode) + len(q.dataLen) + len(strings.ReplaceAll(q.encodedData, " ", "")) + len(q.terminator))
}

func (q *qrCode) getTerminator() {
	bitsToAdd := uint8(q.bitsRequired - q.totalLength)
	if bitsToAdd > 0 && bitsToAdd < 4 {
		q.terminator = q.padZeros(bitsToAdd, "")
	} else if q.totalLength < q.bitsRequired {
		q.terminator = "0000"
	}
}

func corePaddingToStringList(s string) (out [8]string) {
	for i, v := range s {
		out[i] = string(v)
	}
	return
}

func (q *qrCode) finalizeString() {
	coreWordsFmt := strings.ReplaceAll(q.coreWordsString, " ", "")
	q.lenCoreWords = uint(len(coreWordsFmt))
	// 236
	// 17
	padOne := fmt.Sprintf("%b", 236)
	padTwo := fmt.Sprintf("%b", 17)
	padTwo = q.padZeros(8, padTwo)
	padFlag := false
	q.finalString = q.coreWordsString
	for {
		if len(coreWordsFmt) < int(q.bitsRequired) {
			if padFlag {
				q.finalString += " " + padTwo
				coreWordsFmt += padOne
				q.coreWords = append(q.coreWords, corePaddingToStringList(padOne))
				padFlag = !padFlag
			} else {
				q.finalString += " " + padOne
				coreWordsFmt += padTwo
				q.coreWords = append(q.coreWords, corePaddingToStringList(padTwo))
				padFlag = !padFlag

			}
		} else {
			break
		}
	}
	q.finalLength = uint(len(coreWordsFmt))
}

func (q *qrCode) coreWordsToDecimals() {
	for _, v := range strings.Split(q.finalString, " ") {
		uintRes, err := strconv.ParseUint(v, 2, 64)
		if err != nil {
			panic(err)
		}
		bitValueString := strconv.Itoa(int(uintRes))

		q.coreWordsIntStr += bitValueString + " "
		q.coreWordsInt = append(q.coreWordsInt, int(uintRes))
	}
}
func (q *qrCode) InterLeave() {
	//var remainder int
	var message []string
	if q.v.versionNumber == 1 {
		//	remainder = 0
	} else {
		fmt.Println("Not Yet Implemented")
	}
	fullIntMessage := append(q.coreWordsInt, q.ecWords...)
	for _, v := range fullIntMessage {
		binMessage := fmt.Sprintf("%b", v)
		if len(binMessage) < 8 {
			binMessage = q.padZeros(8, binMessage)
		}
		message = append(message, binMessage)
	}
	q.interleavedMessageSlice = message
	q.InterleavedMessagePretty = strings.Join(message, " ")
	q.InterleavedMessage = strings.Join(message, "")
}

func main() {
	q := NewQRCode(1, M, "HELLO WORLD")

	println("Source Data:                      ", q.message)
	println("Data Mode:                        ", q.dataMode)
	println("String Length Encoded:            ", q.dataLen)
	println("String Data Encoded:             ", q.encodedData)
	println("Terminator:                       ", q.terminator)
	println("Bits Lenght Required:             ", q.bitsRequired, " Bits")
	println("String Total Length:              ", q.totalLength)
	println("CoreWords String:                 ", q.coreWordsString)
	println("Length CoreWords String:          ", q.lenCoreWords)
	println("Final String Padded               ", q.finalString)
	println("Final String Length               ", q.finalLength, "Bits")
	println("")
	println("ERROR CORRECTION")
	println("")
	println("Message Polynomial:               ", q.coreWordsIntStr)
	println("Error Correction Blocks Required: ", q.errorCorrection.blocksRequired)
	println("Polynomial Generator String:      ", q.errorCorrection.generatorPolynomialString)
	fmt.Println("String Error Blocks:              ", q.ecWords)
	print("\nInterleaved Message:              \n", q.InterleavedMessagePretty)
	println("")
	print("\nInterleaved Message:              \n", q.InterleavedMessage)
	println("")
	print("\nMatrix Finders\n")
	/*	PrintV1(q.matrixFinders)
		print("\nMatrix With Separators\n")
		PrintV1(q.matrixSeparators)
		print("\nMatrix With Timing Line\n")
		PrintV1(q.matrixTiming)
		print("\nMatrix With Dark Module\n")
		PrintV1(q.matrixDarkBlock)
		print("\nData Reserved Matrix\n")
		PrintV1(q.matrixReserved)*/
	print("\nFilled Matrix\n")
	PrintV1(q.matrixData)
	print("\n Best Matrix \n")
	PrintV1(q.bestMatrix)
	print("\n Complete Matrix \n")
}
