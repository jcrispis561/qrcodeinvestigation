package main

import (
	"math"
)

// Constant penalty
const (
	penaltyWeight1 = 3
	penaltyWeight2 = 3
	penaltyWeight3 = 40
	penaltyWeight4 = 10
)

type Protected struct {
	value     int
	protected bool
}

func switchBit(x, y int, matrix *[21][21]Protected) {
	if matrix[x][y].value == 1 && !(matrix[x][y].protected) {
		(*matrix)[x][y].value = 2
	} else if matrix[x][y].value == 2 && !(matrix[x][y].protected) {
		(*matrix)[x][y].value = 1
	} else {
		return
	}

}

func applyMask(matrix [21][21]Protected, i int) [21][21]Protected {
	newMatrix := matrixCopy(matrix)
	switch i {
	case 0:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if (x+y)%2 == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	case 1:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if (x)%2 == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	case 2:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if (y)%3 == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	case 3:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if (x+y)%3 == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	case 4:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if int(math.Floor(float64(x)/2))+int(math.Floor(float64(y/3)))%2 == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	case 5:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if ((x*y)%2)+((x*y)%3) == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	case 6:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if (((x*y)%2)+((x*y)%3))%2 == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	case 7:
		for x := 0; x < 21; x++ {
			for y := 0; y < 21; y++ {
				if (((x+y)%2)+((x*y)%3))%2 == 0 {
					switchBit(x, y, &newMatrix)
				}
			}
		}
		return newMatrix
	}
	return newMatrix
}

func penalty1(matrix [21][21]Protected) int {
	penalty := 0

	for x := 0; x < 21; x++ {
		lastValue := matrix[x][0].value
		count := 1

		for y := 1; y < 21; y++ {
			v := matrix[x][y].value

			if v != lastValue {
				count = 1
				lastValue = v
			} else {
				count++
				if count == 6 {
					penalty += penaltyWeight1 + 1
				} else if count > 6 {
					penalty++
				}
			}
		}
	}

	for y := 0; y < 21; y++ {
		lastValue := matrix[0][y].value
		count := 1

		for x := 1; x < 21; x++ {
			v := matrix[x][y].value

			if v != lastValue {
				count = 1
				lastValue = v
			} else {
				count++
				if count == 6 {
					penalty += penaltyWeight1 + 1
				} else if count > 6 {
					penalty++
				}
			}
		}
	}

	return penalty
}

func penalty2(matrix [21][21]Protected) int {
	penalty := 0

	for y := 1; y < 21; y++ {
		for x := 1; x < 21; x++ {
			topLeft := matrix[x-1][y-1].value
			above := matrix[x][y-1].value
			left := matrix[x-1][y].value
			current := matrix[x][y].value

			if current == left && current == above && current == topLeft {
				penalty++
			}
		}
	}

	return penalty * penaltyWeight2
}

func penalty3(matrix [21][21]Protected) int {
	// 2 black
	// 1 white
	penalty := 0
	for y := 0; y < 21; y++ {
		for x := 0; x < 21; x++ {
			if x+6 < 21 &&
				matrix[x][y].value == 2 &&
				matrix[x+1][y].value == 1 &&
				matrix[x+2][y].value == 2 &&
				matrix[x+3][y].value == 2 &&
				matrix[x+4][y].value == 2 &&
				matrix[x+5][y].value == 1 &&
				matrix[x+6][y].value == 2 {
				if x+10 < 21 &&
					matrix[x+7][y].value == 1 &&
					matrix[x+8][y].value == 1 &&
					matrix[x+9][y].value == 1 &&
					matrix[x+10][y].value == 1 {
					penalty += 40
				}
				if x-4 >= 0 &&
					matrix[x-4][y].value == 1 &&
					matrix[x-3][y].value == 1 &&
					matrix[x-2][y].value == 1 &&
					matrix[x-1][y].value == 1 {
					penalty += 40
				}
			}
			if y+6 < 21 &&
				matrix[x][y].value == 2 &&
				matrix[x][y+1].value == 1 &&
				matrix[x][y+2].value == 2 &&
				matrix[x][y+3].value == 2 &&
				matrix[x][y+4].value == 2 &&
				matrix[x][y+5].value == 1 &&
				matrix[x][y+6].value == 2 {
				if y+10 < 21 &&
					matrix[x][y+7].value == 1 &&
					matrix[x][y+8].value == 1 &&
					matrix[x][y+9].value == 1 &&
					matrix[x][y+10].value == 1 {
					penalty += 40
				}
				if y-4 >= 0 &&
					matrix[x][y-4].value == 1 &&
					matrix[x][y-3].value == 1 &&
					matrix[x][y-2].value == 1 &&
					matrix[x][y-1].value == 1 {
					penalty += 40
				}
			}
		}
	}
	return penalty
}

func penalty4(matrix [21][21]Protected) int {
	numModules := 21 * 21
	numDarkModules := 0

	for x := 0; x < 21; x++ {
		for y := 0; y < 21; y++ {
			if v := matrix[x][y]; v.value == 2 {
				numDarkModules++
			}
		}
	}

	numDarkModuleDeviation := numModules/2 - numDarkModules
	if numDarkModuleDeviation < 0 {
		numDarkModuleDeviation *= -1
	}

	return penaltyWeight4 * (numDarkModuleDeviation / (numModules / 20))
}

func matrixCopy(matrix [21][21]Protected) [21][21]Protected {
	var dst [21][21]Protected
	for i := 0; i < 21; i++ {
		for x := 0; x < 21; x++ {
			dst[i][x] = matrix[i][x]
		}
	}

	return dst
}

func getPenaltyScore(matrix [21][21]Protected) int {
	return penalty1(matrix) + penalty2(matrix) + penalty3(matrix) + penalty4(matrix)
}

func (q *qrCode) evalMask() {
	matrix := q.matrixData
	eval := [8]int{}
	masks := [8][21][21]Protected{}

	for i := 0; i < 8; i++ {
		m := applyMask(matrix, i)
		masks[i] = m
		eval[i] = getPenaltyScore(m)
	}

	min := getMinimum(eval)
	q.bestMatrix = masks[min]
	q.bestIndex = min
	q.bestScore = eval[min]
}

func getMinimum(a [8]int) int {
	min := a[0]
	i := 0
	for i, v := range a {
		if v < min {
			min = v
			i = i
		}
	}

	return i
}
