package main

func AddDarkModule(m *[21][21]Protected, x, y int) {
	(*m)[x][y] = Protected{2, true}
}

func ReserveFormatInfoArea(m *[21][21]Protected, dim int) {
	for pos := 1; pos < 9; pos++ {
		// Timing Line
		if pos == 6 {
			(*m)[8][dim-pos] = Protected{3, true}
			(*m)[dim-pos][8] = Protected{3, true}
			continue
		}

		if pos == 8 {
			(*m)[8][pos] = Protected{3, true}     // Arriba Izquierda Col
			(*m)[pos][8] = Protected{3, true}     // Arriba izquierda Row
			(*m)[dim-pos][8] = Protected{3, true} // Arribla Derecha Row
			continue
		}

		(*m)[8][pos] = Protected{3, true}     // Arriba Izquierda Col
		(*m)[pos][8] = Protected{3, true}     // Arriba izquierda Row
		(*m)[dim-pos][8] = Protected{3, true} // Arribla Derecha Row
		(*m)[8][dim-pos] = Protected{3, true} // Abajo Izquierda Col
	}
}

func MakeFinderPatterns(m *[21][21]Protected, top, left int) {
	x, y := top, left
	// Caminando por el borde negro
	for i := 0; i < 24; i++ {
		(*m)[x][y] = Protected{2, true}
		if i < 6 {
			x = x + 1
		} else if i < 12 {
			y = y + 1
		} else if i < 18 {
			x = x - 1
		} else if i < 24 {
			y = y - 1
		}
	}

	// Haciendo el interior blanco
	x, y = top+1, left+1
	for i := 0; i < 16; i++ {
		m[x][y] = Protected{1, true}
		if i < 4 {
			x = x + 1
		} else if i < 8 {
			y = y + 1
		} else if i < 12 {
			x = x - 1
		} else if i < 16 {
			y = y - 1
		}
	}

	// Haciendo el centro negro
	for x := left + 2; x < left+5; x++ {
		for y := top + 2; y < top+5; y++ {
			(*m)[x][y] = Protected{2, true}
		}
	}
}

func addSeparator(m *[21][21]Protected, x, y, dimension int) {
	// arriba a la izquierda
	if x == 7 && y == 7 {
		for pos := 0; pos < 8; pos++ {

			(*m)[x][pos] = Protected{1, true}
			(*m)[pos][y] = Protected{1, true}

		}
		return
	}

	// arriba a la derecha
	if x == dimension-8 && y == 7 {
		for pos := 0; pos < 8; pos++ {
			for pos := 0; pos < 8; pos++ {
				(*m)[x][y-pos] = Protected{1, true}
				(*m)[x+pos][y] = Protected{1, true}

			}
			return
		}
	}

	// abajo a la izquierda
	if x == 7 && y == dimension-8 {
		for pos := 0; pos < 8; pos++ {
			(*m)[x][y+pos] = Protected{1, true}
			(*m)[x-pos][y] = Protected{1, true}
		}
		return
	}
}

func addTimingLine(m *[21][21]Protected, dimension int) {
	for pos := 8; pos < dimension-8; pos++ {
		if pos%2 == 0 {
			(*m)[6][pos] = Protected{2, true}
			(*m)[pos][6] = Protected{2, true}
		} else {
			(*m)[6][pos] = Protected{1, true}
			(*m)[pos][6] = Protected{1, true}
		}
	}
}

func fillIntoMatrix(m *[21][21]Protected, message string, dimension int) {
	var (
		messageArr     = []byte(message)
		shift          = 1
		direction      = 1
		l              = 0
		u              = 0
		g              = 1
		cap1           = (dimension - 9) * 2
		cap2           = (dimension) * 2
		count          = 0
		counter        = 0
		count3         = 0
		maxCount   int = ((dimension - 17) / 2) + 4
		maxCount3      = (dimension - 17) * 2
		maxcol     int = ((dimension - 7) / 2) - 1
	)

	for pos, i := range messageArr {
		if pos > ((dimension - 9) * 8) {
			counter = counter + 1
		}

		if pos > 0 && pos%cap1 == 0 && count < 4 {
			count = count + 1
			shift = shift + 2
			direction = -direction
			g = -g
			u = u + g
		} else if count >= 4 && count < maxCount && counter%cap2 == 0 {
			count = count + 1
			shift = shift + 2
			direction = -direction
			g = -g
			u = u + g
		}

		if ((dimension - 1) - u) == 6 {
			u = u + (1 * direction)
			counter = counter + 2
		}

		if count == maxcol && counter%cap2 == 0 {
			u = u + 8
			counter = counter + 14
			count = count + 1
		}

		if count3 == maxCount3 {
			shift = shift + 3
			direction = -direction
			g = -g
			u = u + g
		} else if count3 > maxCount3 && count3%maxCount3 == 0 {
			shift = shift + 2
			direction = -direction
			g = -g
			u = u + g
		}

		if count >= maxcol {
			count3 = count3 + 1
		}

		if string(i) == "0" {

			(*m)[dimension-1-u][dimension-shift-l] = Protected{1, false}
		} else if string(i) == "1" {
			(*m)[dimension-1-u][dimension-shift-l] = Protected{2, false}
		}

		l = (l + 1) % 2
		u = u + (pos%2)*direction

	}
}

func MakeMatrix() [21][21]Protected {
	var matrix [21][21]Protected
	for row := 0; row < 21; row++ {
		for col := 0; col < 21; col++ {
			matrix[row][col] = Protected{0, false}
		}
	}

	return matrix
}

func PrintV1(matrix [21][21]Protected) {
	println("")
	println("")
	for row := 0; row < 21; row++ {
		for col := 0; col < 21; col++ {
			if matrix[col][row].value == 0 {
				print(" ")
			}
			if matrix[col][row].value == 1 {
				print("  ")
			} else if matrix[col][row].value == 2 {
				print("██")
			}
		}
		println("")
	}
}
