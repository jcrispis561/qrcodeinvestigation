package main

import (
	"io/ioutil"
)

var AlphaTable map[string]int8 = make(map[string]int8)

func makeTable() {
	dat, err := ioutil.ReadFile("./alpha.table")
	if err != nil {
		panic(err)
	}
	datString := string(dat)
	var counter int8
	var key string
	var value int8
	var keyCount int8
	for _, v := range datString {
		counter++
		if counter == 1 {
			key = string(v)
		} else if counter == 2 {
			continue
		} else if counter == 3 {
			value = keyCount
			keyCount++
			if err != nil {
				panic(err)
			}
		} else if v == '\n' {
			AlphaTable[key] = value
			counter = 0
		}

	}
}
