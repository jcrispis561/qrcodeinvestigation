package main

type infoBits struct {
	lastBits      string
	maskV         string
	generatorPoly []int
}

func (q *qrCode) encodeInfoBits() {
	var maskV []int
	var xorArray []int
	var xorArray2 []int
	var xorArray3 []int
	var xorArray4 []int
	var lastBits []int
	var modeCode []int

	switch q.errorMode {
	case L:
		lastBits = []int{0, 1}
	case M:
		lastBits = []int{0, 1}
	case Q:
		lastBits = []int{1, 1}
	case H:
		lastBits = []int{1, 0}
	}

	switch q.bestIndex {
	case 0:
		modeCode = []int{0, 0, 1}
	case 1:
		modeCode = []int{0, 1, 0}
	case 2:
		modeCode = []int{0, 1, 1}
	case 3:
		modeCode = []int{1, 0, 0}
	case 4:
		modeCode = []int{1, 0, 1}
	case 5:
		modeCode = []int{1, 1, 0}
	case 6:
		modeCode = []int{1, 1, 0}
	case 8:
		modeCode = []int{1, 0, 0, 0}
	}

	maskV = append(maskV, modeCode...)

	maskV = append(lastBits, maskV...)

	generatorPoly := []int{1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1}

	for {
		if len(maskV) <= 15 {
			maskV = append(maskV, 0)
		} else {
			break
		}
	}
	maskV = trimLeadingZeros(maskV)

	for {
		if len(generatorPoly) < len(maskV) {
			generatorPoly = append(generatorPoly, 0)
		} else {
			break
		}
	}

	for i := 0; i < len(generatorPoly); i++ {
		xorArray = append(xorArray, maskV[i]^generatorPoly[i])
	}

	xorArray = trimLeadingZeros(xorArray)
	generatorPoly = []int{1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1}

	for {
		if len(generatorPoly) < len(xorArray) {
			generatorPoly = append(generatorPoly, 0)
		} else {

			break
		}
	}
	for i := 0; i < len(xorArray); i++ {
		xorArray2 = append(xorArray2, xorArray[i]^generatorPoly[i])
	}

	xorArray2 = trimLeadingZeros(xorArray2)

	generatorPoly = []int{1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1}

	for {
		if len(generatorPoly) < len(xorArray2) {
			generatorPoly = append(generatorPoly, 0)
		} else {
			break
		}
	}
	for i := 0; i < len(xorArray2); i++ {
		xorArray3 = append(xorArray3, xorArray2[i]^generatorPoly[i])
	}

	xorArray3 = trimLeadingZeros(xorArray3)

	generatorPoly = []int{1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1}

	for i := 0; i < len(xorArray3); i++ {
		xorArray4 = append(xorArray4, xorArray3[i]^generatorPoly[i])
	}

	xorArray4 = trimLeadingZeros(xorArray4)
	for {
		if len(xorArray4) >= 10 {
			break
		} else {
			xorArray4 = append([]int{0}, xorArray4...)

		}
	}

	errCStr := []int{}
	errCStr = append(errCStr, modeCode...)

	errCStr = append(lastBits, errCStr...)

	for _, v := range xorArray4 {
		errCStr = append(errCStr, v)
	}

	xorStandar := []int{1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0}
	finale := []int{}

	for i := 0; i < len(xorStandar); i++ {
		finale = append(finale, errCStr[i]^xorStandar[i])
	}
	newMatrix := q.bestMatrix
	majPos := 14

	for pos := 0; pos < 9; pos++ {
		if pos == 6 {
			continue
		}
		if finale[majPos] == 0 {
			newMatrix[8][pos].value = 1

		} else if finale[majPos] == 1 {
			newMatrix[8][pos].value = 2

		}
		majPos--

	}

	for pos := 7; pos >= 0; pos-- {
		y := 8
		if pos == 6 {
			continue
		}
		if finale[majPos] == 0 {
			newMatrix[pos][y].value = 1

		} else if finale[majPos] == 1 {
			newMatrix[pos][y].value = 2

		}
		majPos--

	}
	y := 8
	x := 13
	for pos := 7; pos <= 14; pos++ {

		if finale[pos] == 0 {
			newMatrix[x][y].value = 1

		} else if finale[pos] == 1 {
			newMatrix[x][y].value = 2

		}

		x++

	}

	y = 14
	x = 8
	for pos := 6; pos >= 0; pos-- {

		if finale[pos] == 0 {
			newMatrix[x][y].value = 1

		} else if finale[pos] == 1 {
			newMatrix[x][y].value = 2

		}

		y++

	}

	q.preOutput = newMatrix

}
