package main

var Exp2Int []int = []int{1}
var Int2Exp map[int]int = make(map[int]int)

func generateLogAntilogTable() {
	for i := 0; i <= 256; i++ {
		Int2Exp[i] = i
	}

	for i := 1; i <= 256; i++ {
		b := Exp2Int[i-1] * 2
		if b >= 256 {
			b = b ^ 285
		}
		Exp2Int = append(Exp2Int, b)
		Int2Exp[b] = i
	}

	delete(Int2Exp, 0)
	Int2Exp[1] = 0

}

func MakeLongDivision(steps int, mPoly, gPoly []int) []int {
	generateLogAntilogTable()
	var newGPoly []int = []int{}

	for i := 0; i <= steps-1; i++ {
		gap := len(mPoly) - len(gPoly)
		m := mPoly[0]
		m = Int2Exp[int(m)]

		if gap > 0 {
			newGPoly = []int{}
			for _, g := range gPoly {
				newGPoly = append(newGPoly, Exp2Int[(g+m)%255])
			}
			newGPoly = append(newGPoly, MakeZeroSlice(gap)...)

		} else {
			newGPoly = []int{}
			for _, g := range gPoly {
				newGPoly = append(newGPoly, Exp2Int[(g+m)%255])
			}

		}

		var blank []int
		if gap < 0 {
			mPoly = append(mPoly, MakeZeroSlice(abs(gap))...)

		}
		for i := 0; i < len(newGPoly); i++ {
			b := mPoly[i] ^ newGPoly[i]
			blank = append(blank, b)
		}
		mPoly = trimLeadingZeros(blank)
	}
	return mPoly
}

func MakeZeroSlice(length int) []int {
	var result []int
	for i := 0; i <= length-1; i++ {
		result = append(result, 0)
	}

	return result
}

func MakeUintZeroSlice(length int) []uint {
	var result []uint
	for i := 0; i <= length-1; i++ {
		result = append(result, 0)
	}

	return result
}

func abs(n int) int {
	if n < 0 {
		return -n
	}
	return n
}

func remove(slice *[]int, s int) {
	*slice = append((*slice)[:s], (*slice)[s+1:]...)
}

func trimLeadingZeros(slice []int) []int {
	for _, v := range slice {
		if v == 0 {
			slice = slice[1:]
		} else {
			break
		}
	}
	return slice
}
